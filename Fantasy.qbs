QtApplication {
    cpp.cxxLanguageVersion: ["c++2a"]
    cpp.includePaths: ["wren/src/include", "wren/src/optional", "wren/src/vm", "src"]
    files: [
        "src/*.cpp",
        "wren/src/vm/*.c",
        "wren/src/optional/*.c"
    ]
    Group {
        files: ["stdlib/*"]
        fileTags: "qt.core.resource_data"
        Qt.core.resourceSourceBase: "stdlib"
        Qt.core.resourcePrefix: "/stdlib"
    }
    Depends { name: "Qt"; submodules: ["gui"] }
}