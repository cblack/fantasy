#pragma once

#include <QList>
#include <QPair>
#include <QSharedPointer>
#include <QString>

#include "wren.hpp"

using wrenFunc = void(*)(WrenVM*);
using vPointerFunc = void(*)(void*);

struct Meth
{
    QString name;
    bool isStatic;

    virtual ~Meth() {}

    virtual wrenFunc func() const = 0;
};

struct CData
{
    QString module;
    QString name;
    QList<QSharedPointer<Meth>> methods;

    virtual ~CData() {}

    virtual wrenFunc constructor() const = 0;
    virtual vPointerFunc destructor() const = 0;
};

template<typename T>
struct ClassData : public CData
{
    ClassData(QString mod, QString name, QList<QSharedPointer<Meth>> methods)
    {
        this->module = mod;
        this->name = name;
        this->methods = methods;
    }
    wrenFunc constructor() const override {
        return [](WrenVM* vm) {
            auto data = wrenSetSlotNewForeign(vm, 0, 0, sizeof(T));
            new (data) T;
        };
    }
    vPointerFunc destructor() const override {
        return [](void* dat) {
            T* inst = (T*) dat;
            inst->~T();
        };
    };
};

template<typename T, void(fn)(T*, WrenVM*)>
struct Method : public Meth
{
    Method(QString name, bool isStatic)
    {
        this->name = name;
        this->isStatic = isStatic;
    }
    wrenFunc func() const override {
        return [](WrenVM* vm) {
            auto dat = (T*) wrenGetSlotForeign(vm, 0);
            fn(dat, vm);
        };
    }
};

#define NewMethod(T, Impl, name, isStatic) QSharedPointer<Meth>( new Method<T, Impl>( name, isStatic ) )

