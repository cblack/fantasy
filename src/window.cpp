#include <QOpenGLWindow>
#include <QDebug>

#include "window.hpp"

namespace Display
{

constexpr auto test = [](Window*, WrenVM*) {
    qDebug() << "mu!";
};

ClassData<Window> window = ClassData<Window>(
    "Display",
    "Window",
    {
        NewMethod(Window, test, "test()", false)
    }
);

}