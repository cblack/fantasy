#include <QCoreApplication>
#include <QDebug>
#include <QFile>
#include <QScopeGuard>

#include "classdata.hpp"

#include "window.hpp"
#include "wren.h"
#include "wren.hpp"

static void writeFn(WrenVM *vm, const char *text)
{
    Q_UNUSED(vm)

    printf("%s", text);
}

static void errorFn(WrenVM *vm, WrenErrorType errorType, const char *module, const int line, const char *msg)
{
    Q_UNUSED(vm)

    switch (errorType) {
    case WREN_ERROR_COMPILE:
        qDebug() << qPrintable(QString("[%1:%2] [Compilation Error] %3").arg(module).arg(line).arg(msg));
        break;
    case WREN_ERROR_STACK_TRACE:
        qDebug() << qPrintable(QString("[%1:%2] in %3").arg(module).arg(line).arg(msg));
        break;
    case WREN_ERROR_RUNTIME:
        qDebug() << qPrintable(QString("[%1] [Runtime Error]").arg(msg));
        break;
    }
}

static QMap<QString, QByteArray> moduleData;

static WrenLoadModuleResult loadModule(WrenVM* vm, const char* name)
{
    Q_UNUSED(vm)

    auto path = QString(":/stdlib/%1.wren").arg(name);

    if (moduleData.contains(path)) {
        return WrenLoadModuleResult { moduleData[path].constData() };
    }

    if (QFile::exists(path)) {
        QFile f(path);
        Q_ASSERT(f.open(QIODevice::ReadOnly));
        moduleData[path] = f.readAll();

        return WrenLoadModuleResult {
            .source = moduleData[path].constData()
        };
    }

    return WrenLoadModuleResult{};
}

static QList<CData*> classes = {
    &Display::window,
};

static WrenForeignClassMethods bindForeignClass(WrenVM* vm, const char* module, const char* className)
{
    Q_UNUSED(vm)
    auto mod = QString::fromLocal8Bit(module), cname = QString::fromLocal8Bit(className);

    for (const auto& cls : classes) {
        if (cls->module == mod && cls->name == cname) {
            return WrenForeignClassMethods {
                .allocate = cls->constructor(),
                .finalize = cls->destructor()
            };
        }
    }

    return WrenForeignClassMethods{};
}

static WrenForeignMethodFn bindForeignMethodFn(WrenVM* vm, const char* module, const char* className, bool isStatic, const char* signature)
{
    Q_UNUSED(vm)
    auto mod = QString::fromLocal8Bit(module), name = QString::fromLocal8Bit(className), sig = QString::fromLocal8Bit(signature);

    for (const auto& cls : classes) {
        if (cls->module == mod && cls->name == name) {
            for (const auto& meth : cls->methods) {
                if (meth->name == signature && meth->isStatic == isStatic) {
                    return meth->func();
                }
            }
        }
    }

    return nullptr;
}

// clang-format on
#define defer(fn) auto cleanup ## __LINE__ = qScopeGuard([=] { fn; })
// clang-format off

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    WrenConfiguration cfg;
    wrenInitConfiguration(&cfg);
        cfg.writeFn = writeFn;
        cfg.errorFn = errorFn;
        cfg.loadModuleFn = loadModule;
        cfg.bindForeignClassFn = bindForeignClass;
        cfg.bindForeignMethodFn = bindForeignMethodFn;

    WrenVM *vm = wrenNewVM(&cfg);
    defer(wrenFreeVM(vm));

    QFile f("os.wren");
    if (!f.open(QIODevice::ReadOnly)) {
        return -1;
    }

    auto data = f.readAll();
    wrenInterpret(vm, "main", data.constData());
}
